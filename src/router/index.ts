import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

import Home from '../views/Home.vue'
import GridLayout from '../views/GridLayout.vue'
import Buttons from '../views/Buttons.vue'
import Modal from '../views/Modal.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/grid-layout',
    name: 'Grid Layout',
    component: () => import(/* webpackChunkName: "about" */ '../views/GridLayout.vue')
  },
  {
    path: '/buttons',
    name: 'Buttons',
    component: () => import(/* webpackChunkName: "about" */ '../views/Buttons.vue')
  },
  {
    path: '/modal',
    name: 'Modal',
    component: () => import(/* webpackChunkName: "about" */ '../views/Modal.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
