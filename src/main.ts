import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import VueHighlightJS from 'vue-highlight.js';
import 'vue-highlight.js/lib/allLanguages';
import 'highlight.js/styles/default.css';

import './scss/main.scss';

Vue.use(VueHighlightJS);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
